<?php

// Kickstart the framework
$f3=require('lib/base.php');

$f3->set('DEBUG',1);
$f3->set('db', new DB\SQL('mysql:host=localhost;port=3306;dbname=jauyeung_f3blog','jauyeung_f3blog','dVzwJfe2NySKeBZ4U70N'));
if ((float)PCRE_VERSION<7.9)
	trigger_error('PCRE version is out of date');

// Load configuration
$f3->config('config.ini');

include('comments.php');

$f3->route('GET /',
	function($f3) {		
		$db = $f3->get('db');
		$posts=new DB\SQL\Mapper($db,'posts');
		$f3->set('posts', $posts->find());		
		echo Template::instance()->render('index.htm');
	}
);

$f3->route('GET /getpostbyid/@id',
	function($f3) {		
		$db = $f3->get('db');	
		$rows = $db->exec("select * from posts where id = {$f3->get('PARAMS.id')}");		
		$f3->set('posts', $rows);				
		$comments = $db->exec("select c.id, c.content, c.commentdate from posts p inner join comments c on p.id = c.postid where p.id = {$f3->get('PARAMS.id')}");	
		$f3->set('comments', $comments);			
		echo Template::instance()->render('postbyid.htm');
	}
);

$f3->route('GET /addpostform',
	function($f3) {		
		echo View::instance()->render('addpostform.htm');
	}
);

$f3->route('POST /addpost',
	function($f3) {		
		$db = $f3->get('db');
		$posts=new DB\SQL\Mapper($db,'posts');
		$posts->title =  $f3->get('POST.title');
		$posts->content = $f3->get('POST.content');
		$posts->postdate = date('Y-m-d H:i:s', time());
		$posts->save();
		echo $f3->reroute('/');
	}
);

$f3->route('GET /editpostform/@id',
	function($f3) {		
		$f3->set('postid', $f3->get('PARAMS.id'));
		echo Template::instance()->render('editpostform.htm');
	}
);

$f3->route('POST /editpost',
	function($f3) {	
		$db = $f3->get('db');	
		$posts=new DB\SQL\Mapper($db,'posts');		
		$id = $f3->get('POST.postid');
		$posts->load(array('id=?',$id));
		$posts->title = $f3->get('POST.title');
		$posts->content = $f3->get('POST.content');
		$posts->update();
		echo $f3->reroute("/getpostbyid/$id");
	}
);

$f3->route('GET /deletepost/@id',
	function($f3) {		
		$db = $f3->get('db');
		$id = $f3->get('PARAMS.id');
		$comments=new DB\SQL\Mapper($db,'comments');	
		$comments->load(array('postid=?',$id));
		$comments->erase();
		$posts=new DB\SQL\Mapper($db,'posts');				
		$posts->load(array('id=?',$id));
		$posts->erase();
		echo $f3->reroute('/');
	}
);




$f3->run();
