drop database if exists f3blog;
create database f3blog;
use f3blog;
create table posts (
	id int primary key not null auto_increment,
	title varchar(300),
	postdate datetime,
	content text	
);

create table comments(
	id int primary key not null auto_increment,
	content varchar(500),
	commentdate datetime,
	postid int,
	foreign key (postid) references posts(id)	
);

insert into posts values('', 'Title', now(), 'Text');