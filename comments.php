<?php 

$f3->route('POST /addcomment',
	function($f3) {		
		$db = $f3->get('db');
		$id = $_POST['postid'];
		$comments=new DB\SQL\Mapper($db,'comments');				
		$comments->content = $_POST['comment'];
		$comments->commentdate = date('Y-m-d H:i:s', time());
		$comments->postid = $id;
		$comments->save();
		echo $f3->reroute("/getpostbyid/$id");
	}
);

$f3->route('GET /deletecomment/@id',
	function($f3) {		
		$db = $f3->get('db');				
		$comments=new DB\SQL\Mapper($db,'comments');
		$id = $f3->get('PARAMS.id');
		$comments->load(array('id=?',$id));
		$postid = $comments->postid;
		$comments->erase();
		echo $f3->reroute("/getpostbyid/$postid");
	}
);

?>